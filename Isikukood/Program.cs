﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isikukood
{
    enum Sugu { Naine, Mees}

    static class Program
    {
        static void Main(string[] args)
        {
            string isikukood = "35503070211";

            Console.WriteLine($"minu isikukood on {isikukood} ma olen {isikukood.ToSugu()} ja sündinud {isikukood.ToDatetime()}");
            Console.WriteLine(isikukood.Check() ? "õige" : "vale");
 

        }

        static bool Check(this string isikukood)
        {
            int i = 0;
    
            int kj =
            isikukood.Substring(0, 10)
                .Select(x => x - '0')
                .ToArray()
                .Select(x => x * ((i++ % 9)+1))  //.Prindi()
                .Sum()  //.Prindi()
                % 11;
            if (kj == 10)
            {
                i = 2;
                kj = isikukood.Substring(0,10).ToArray()
                    .Select( x => x * ((i++ % 9) + 1))
                .Sum() % 11 % 10;
            }



            return kj == isikukood[10]-'0';
        }

        static int Prindi(this int i)
        {
            Console.WriteLine();
            Console.WriteLine(i);
            return i;
        }
        static IEnumerable<int> Prindi(this IEnumerable<int> m)
        {
            Console.WriteLine();
            foreach (var x in m) { Console.Write($"{x} "); yield return x; }
           
        }

        static Sugu ToSugu(this String isikukood)
        {
            return (isikukood[0] - '0') % 2 == 0 ? Sugu.Naine : Sugu.Mees;
        }


        static DateTime ToDatetime(this string isikukood)
        {
            DateTime x;
            
        DateTime.TryParse(
            (((isikukood[0]-'0' - 1) / 2 + 18).ToString() +
                isikukood.Substring(1, 2) + "-" +
                isikukood.Substring(3, 2) + "-" +
                isikukood.Substring(5, 2))
                , out x)
                ;
            return x;
        }

    }
    
}
